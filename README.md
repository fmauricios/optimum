# Optimum

*This test was created on Arch Linux environment.*

- Install Ruby 2.4.2
- cd optimum
- ```bundle install```
- Execute ```ruby app.rb```

## Example

```
Input:

Choose your origin city G
Choose your destiny city D

Output:

Optimum route
["G", 86, 94, 100, 48, 122, 52, -1, 76, 60, 32, "D"]

Visited
[["0-7", true], ["1-7", true], ["2-7", true], ["2-6", true], ["2-5", true], ["2-4", true], ["2-3", true], ["3-3", true], ["3-2", true], ["3-1", true], ["4-1", true], ["4-0", true]]
```
