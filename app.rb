require 'json'
require 'colorize'
require 'tty-prompt'

# define matrix

matrix = [
  ['-', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'],
  ['A', -1, 106, 60, 32, 46, 116, 60, 39, 105, 100],
  ['B', 120, -1, 76, 106, 118, 31, 66, 131, 108, 78],
  ['C', 117, 52, -1, 91, 36, 54, 102, 68, 99, 38],
  ['D', 46, 122, 135, -1, 128, 129, 110, 121, 37, 115],
  ['E', 83, 48, 119, 97, -1, 103, 133, 43, 122, 47],
  ['F', 143, 100, 87, 138, 150, -1, 49, 71, 98, 99],
  ['G', 86, 94, 105, 97, 60, 145, -1, 33, 138, 114],
  ['H', 41, 42, 127, 57, 47, 70, 71, -1, 45, 144],
  ['I', 75, 140, 67, 46, 61, 46, 125, 148, -1, 49],
  ['J', 144, 38, 116, 109, 102, 120, 128, 64, 125, -1]
]

puts "Original Matrix \n\n".yellow
puts matrix.to_a.map(&:inspect)

choices = ('A'..'J').to_a

puts "\n\n"

# read user input

prompt = TTY::Prompt.new

start_option = prompt.select("Choose your origin city", choices, help: "(Bash keyboard)", marker: '>')
stop_option = prompt.select("Choose your destiny city", choices, help: "(Bash keyboard)", marker: '>')

if start_option == stop_option
  puts "Origin city cannot be the same that destination city".red
  return
end

y = choices.each_index.select{ |i| choices[i] == start_option }
x = choices.each_index.select{ |i| choices[i] == stop_option }

# set user input
start = {x: 0, y: y[0] + 1}
stop  = {x: x[0] + 1, y: 0}

def find_routes(matrix, results, route, current, stop)
  # out of bounds
  return results if results[:min] && results[:min] < route['total']
  return results if current[:x] < 0 || current[:y] < 0
  return results if current[:y] >= matrix.length || current[:x] >= matrix[0].length
  return results if route['weights'].length > (matrix.length  * 2)

  position_key = "#{current[:x]}-#{current[:y]}"
  position_value = matrix[current[:y]][current[:x]]

  # already visited
  return results if route['visited'][position_key]

  # reached stop
  if current == stop
    # add current position to weights + visited
    route['weights'] << position_value
    route['visited'][position_key] = true

    results[:min] = route['total']
    results[:routes] << route
    return results
  end

  # invalid position
  return results if route['weights'].size > 0 && position_value =~ /[A-Z]/

  # add current position to weights + visited
  route['weights'] << position_value
  route['total'] += position_value if position_value.is_a?(Integer)
  route['visited'][position_key] = true

  # try each direction
  results = find_routes(matrix, results, copy(route), {x: current[:x] - 1, y: current[:y]}, stop)
  results = find_routes(matrix, results, copy(route), {x: current[:x], y: current[:y] - 1}, stop)
  results = find_routes(matrix, results, copy(route), {x: current[:x] + 1, y: current[:y]}, stop)
  results = find_routes(matrix, results, copy(route), {x: current[:x], y: current[:y] + 1}, stop)

  results
end

def copy(hash)
  JSON.parse(hash.to_json)
end

results = find_routes(matrix, {min: false, routes: []}, {'weights' => [], 'total' => 0, 'visited' => {}}, start, stop)

weights = []
visited = []

results[:routes].each do |route|
  weights << route['weights']
  visited << route['visited']
end

puts "\n"
puts "Optimum route".blue
puts weights.last.to_a.inspect
puts "\n"
puts "Visited".blue
puts visited.last.to_a.inspect

weights.last.to_a.shift
weights.last.to_a.pop

total_weights = weights.last.to_a.sum

puts "\n"
puts "Total cost: #{total_weights}".blue
